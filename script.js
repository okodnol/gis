// инициализация и добавление карты во время загрузки
function initMap() {
	
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 1,
		center: {lat: 0, lng: 0}
	});

	var token = '1626661799.ba4c844.7097f9ce0ae84981a2b7d39910cc2430', num_photos = 33;
	var markers = [], infowindows = [];

	$("#btn").on('click', function(){

		var input = $("#input").val();

		if (input != '') {
			clear();
			var hashtag=input;

			$.ajax({
				url: 'https://api.instagram.com/v1/tags/' + hashtag + '/media/recent',
				dataType: 'jsonp',
				type: 'GET',
				data: {access_token: token, count: num_photos},
				success: function(data) {
					for(x in data.data) {
						var url = data.data[x].images.standard_resolution.url;
						if (data.data[x].location != null) {
							var coord = new google.maps.LatLng(data.data[x].location.latitude, data.data[x].location.longitude);
							createMarker(x, coord, url);
						}
						else {
							$('#list').append('<li class="list-item"><a href="'+url+'" target="_blank"><img src="'+url+'"></a></li>');
						} 
					}
				},
				error: function(data){
					console.log(data);
				}
			});
		}
	})

	function createMarker(i, coord, url) {
		markers[i] = new google.maps.Marker({
				position: coord,
				map: map
		});
		infowindows[i] = new google.maps.InfoWindow({
				content: '<div class="popup-item"><a href="'+url+'" target="_blank"><img src="'+url+'"></a></div>'
		});
		google.maps.event.addListener(markers[i], 'click', function() {
				infowindows[i].open(map,markers[i]);
		});
	}

	function clear() {
		$.each(markers, function(index, value) {
			if (value) {
				value.setMap(null);
			}
		});
		markers=[];
		infowindows=[];
		$('#list').empty();
	}
}